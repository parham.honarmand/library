package View;

import controller.LibraryController;
import controller.UserController;
import model.User;
import utility.Utilities;

public class Register {

    public void register() {
        String name;
        String username;
        String password;
        String nationalCode;
        Long budget;

        System.out.println("Register : ");

        System.out.println("please enter your name");
        name = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your username");
        username = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your password");
        password = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your national code");
        nationalCode = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your budget");
        budget = Utilities.getInstance().getLongFromUser();

        User user = new User(name, username, password, nationalCode, String.valueOf(System.currentTimeMillis()), budget);

        LibraryController.getInstance().userController.register(user);
        UserController.currentUser = user;
        System.out.println();
    }
}
