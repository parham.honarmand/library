package model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Borrow {

    public Long id;
    public Long user_id;
    public Long book_id;
    public String start;
    public String end;

    public Borrow(Long user_id, Long book_id, String end){
        this.user_id = user_id;
        this.book_id = book_id;
        start = String.valueOf(System.currentTimeMillis());
        this.end = end;
    }

    public Borrow(Long user_id, Long book_id,String start, String end){
        this.user_id = user_id;
        this.book_id = book_id;
        this.start = start;
        this.end = end;
    }
}
