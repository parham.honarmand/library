package View;

import controller.LibraryController;
import controller.UserController;
import model.Book;
import utility.Utilities;

import java.util.List;

public class Main {
    static Login login = new Login();
    static Register register = new Register();
    static BorrowView borrowView = new BorrowView();

    public static void main(String[] args) {
        firstMenu();
    }

    public static void firstMenu() {
        int number;

        System.out.println("welcome");
        System.out.println("1 - register");
        System.out.println("2 - login");
        System.out.println("3 - add a book");
        System.out.println("enter a number : ");

        number = Utilities.getInstance().getIntFromUser();

        switch (number) {
            case 1:
                register.register();
                firstMenu();
                break;
            case 2:
                login.login();
                secondMenu();
                break;
            case 3:
                addBook();
                firstMenu();
                break;
            default:
                System.out.println("invalid number");
        }
    }

    private static void secondMenu() {
        int number;

        System.out.println("hello " + UserController.currentUser.getName());

        System.out.println("1 - borrow");
        System.out.println("2 - return");
        System.out.println("3 - back to first menu");
        System.out.println("enter a number : ");

        number = Utilities.getInstance().getIntFromUser();

        switch (number) {
            case 1:
                borrowView.borrow();
                secondMenu();
                break;
            case 2:
                borrowView.refund();
                secondMenu();
                break;
            case 3:
                firstMenu();
                break;
            default:
                System.out.println("invalid number");
        }
    }

    public static void addBook() {

        System.out.println("Add Book : ");

        System.out.println("enter book title :");
        String title = Utilities.getInstance().getStringFromUser();

        System.out.println("enter book subject :");
        String subject = Utilities.getInstance().getStringFromUser();

        System.out.println("enter book price :");
        Long price = Utilities.getInstance().getLongFromUser();

        System.out.println("enter book penalty :");
        Long penalty = Utilities.getInstance().getLongFromUser();

        System.out.println("enter book borrow limit day :");
        Integer borrow_limit = Utilities.getInstance().getIntFromUser();

        Book book = new Book(title, subject, price, penalty, borrow_limit);

        LibraryController.getInstance().bookController.insert(book);
        System.out.println();
        showBooks();
    }

    public static List<Book> showBooks() {
        List<Book> bookList = LibraryController.getInstance().bookController.findAll();
        if (bookList.size() == 0)
            System.out.println("there is no book available");

        for (int i = 1; i <= bookList.size(); i++) {
            if (bookList.get(i - 1).isStatus())
                System.out.println(i + " - title : " + bookList.get(i - 1).getTitle() + " , subject : " +
                        bookList.get(i - 1).getSubject());
            else
                System.out.println(i + " - title : " + bookList.get(i - 1).getTitle() + " , subject : " +
                        bookList.get(i - 1).getSubject() + "  not available");
        }
        System.out.println();
        return bookList;
    }

    public static Book chooseBook() {
        Book book;
        int number;

        List<Book> bookList = showBooks();

        System.out.println("enter book number :");
        number = Utilities.getInstance().getIntFromUser();

        if (number == 0 || bookList.size() < number) {
            System.out.println("invalid input");
            secondMenu();
        }

        if (!bookList.get(number - 1).isStatus()) {
            System.out.println("book does not available");
            secondMenu();
        }

        System.out.println();
        return bookList.get(number - 1);
    }
}
