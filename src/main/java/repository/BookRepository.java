package repository;

import model.Book;

public interface BookRepository extends Repository<Book> {
}
