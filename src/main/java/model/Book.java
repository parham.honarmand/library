package model;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@Entity
@Table
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String title;

    @Column
    private String subject;

    @Column
    private Long price;

    @Column
    private Long penalty;

    @Column
    private boolean status;

    @Column
    private Integer borrow_limit;

    public Book(String title, String subject, Long price, Long penalty, Integer borrow_limit) {
        this.title = title;
        this.subject = subject;
        this.price = price;
        this.penalty = penalty;
        this.borrow_limit = borrow_limit;
        status = true;
    }
}
