package View;

import controller.LibraryController;
import controller.UserController;
import model.Book;
import utility.Utilities;

public class BorrowView {

    private static final Integer DAY_MILLISECOND = 86400000;

    public void borrow() {

        String username;

        System.out.println("Borrow a book : ");

        if (UserController.currentUser != null) {
            username = UserController.currentUser.getUsername();
        } else {
            System.out.println("enter your username : ");
            username = Utilities.getInstance().getStringFromUser();
        }


        Book book = Main.chooseBook();
        String end = String.valueOf(System.currentTimeMillis() + book.getBorrow_limit() * DAY_MILLISECOND);

        try {
            if (!LibraryController.getInstance().borrowController.borrow(username, book, end))
                System.out.println("this book is unavailable");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refund() {

        String username;

        System.out.println("Return a book :");

        if (UserController.currentUser != null) {
            username = UserController.currentUser.getUsername();
        } else {
            System.out.println("enter your username");
            username = Utilities.getInstance().getStringFromUser();
        }


        System.out.println("enter book title : ");
        String bookTitle = Utilities.getInstance().getStringFromUser();

        LibraryController.getInstance().borrowController.refund(username, bookTitle);
    }
}
