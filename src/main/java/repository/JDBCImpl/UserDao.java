package repository.JDBCImpl;

import connectionManager.ConnectionManager;
import connectionManager.MySqlConnectionManager;
import model.User;
import repository.UserRepository;
import utility.UserNotFoundException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements UserRepository {
    public ConnectionManager connectionManager = new MySqlConnectionManager();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into users (name, username, password, national_code, creation," +
            " delay_count, status, budget) values (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT_BY_ID_QUERY = "select * from users where books.id = ?";
    private static final String SELECT_BY_USERNAME_QUERY = "select * from users where users.username = ?";
    private static final String SELECT_ALL_QUERY = "select * from users";
    private static final String UPDATE_QUERY = "update users set name = ?, username = ?, password = ?," +
            " national_code = ?, creation = ?, delay_count = ?, status = ?, budget = ? where users.id = ?";
    private static final String DELETE_QUERY = "delete from users where users.id = ?";

    public void insert(User user) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setString(1, user.getName());
        statement.setString(2, user.getUsername());
        statement.setString(3, user.getPassword());
        statement.setString(4, user.getNationalCode());
        statement.setString(5, user.getCreation());
        statement.setInt(6, user.getDelayCount());
        statement.setInt(7, (user.isStatus() ? 1 : 0));
        statement.setLong(8, user.getBudget());
        System.out.println(statement);
        statement.executeUpdate();
    }

    public User searchById(Long id) throws UserNotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new User(resultSet.getLong(1),
                    resultSet.getString(2),
                    resultSet.getString(3), resultSet.getString(4),
                    resultSet.getString(5), resultSet.getString(6),
                    resultSet.getInt(7), resultSet.getBoolean(8), resultSet.getLong(9));

        throw new UserNotFoundException("this user does not exist");
    }

    public List<User> findAll() throws SQLException {
        List<User> userList = new ArrayList<User>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_ALL_QUERY);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            userList.add(new User(resultSet.getLong(1),
                    resultSet.getString(2),
                    resultSet.getString(3), resultSet.getString(4),
                    resultSet.getString(5), resultSet.getString(6),
                    resultSet.getInt(7), resultSet.getBoolean(8), resultSet.getLong(9)));

        return userList;
    }

    public User searchByUsername(String username) throws UserNotFoundException, SQLException {
        System.out.println(SELECT_BY_USERNAME_QUERY);
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_USERNAME_QUERY);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new User(resultSet.getLong(1),
                    resultSet.getString(2),
                    resultSet.getString(3), resultSet.getString(4),
                    resultSet.getString(5), resultSet.getString(6),
                    resultSet.getInt(7), resultSet.getBoolean(8), resultSet.getLong(9));

        throw new UserNotFoundException("this users does not exist");
    }

    public void update(User user) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setString(1, user.getName());
        statement.setString(2, user.getUsername());
        statement.setString(3, user.getPassword());
        statement.setString(4, user.getNationalCode());
        statement.setString(5, user.getCreation());
        statement.setInt(6, user.getDelayCount());
        statement.setInt(7, (user.isStatus() ? 1 : 0));
        statement.setLong(8, user.getBudget());
        statement.setLong(9, user.getId());
        statement.executeUpdate();
    }

    public void delete(Long id) throws SQLException {
        System.out.println(DELETE_QUERY);
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }
}
