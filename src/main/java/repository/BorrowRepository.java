package repository;

import model.Borrow;

import java.sql.SQLException;

public interface BorrowRepository extends Repository<Borrow> {
    void deleteByUserIdAndBookId(Long user_id, Long book_id) throws SQLException;
}
