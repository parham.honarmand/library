package repository;

import repository.HibernateImpl.UserHibernateDao;
import repository.JDBCImpl.BookDao;
import repository.JDBCImpl.BorrowDao;
import repository.JDBCImpl.UserDao;
import utility.RepositoriesType;

public class LibraryRepository {

    public static LibraryRepository libraryRepository;

    private RepositoriesType repositoriesType = RepositoriesType.JDBC;

    private UserDao userDao;
    public BookDao bookDao;
    public BorrowDao borrowDao;
    private UserHibernateDao userHibernateDao;

    public LibraryRepository(){

        switch (repositoriesType) {
            case JDBC:
                userDao = new UserDao();
                bookDao = new BookDao();
                borrowDao = new BorrowDao();
                break;
            case HIBERNATE:
                userHibernateDao = new UserHibernateDao();
                break;
        }
    }

    public static LibraryRepository getInstance(){
        if(libraryRepository == null)
            libraryRepository = new LibraryRepository();
        return libraryRepository;
    }

    public UserRepository getUserDao(){
        switch (repositoriesType){
            case JDBC:
                return userDao;
            case HIBERNATE:
                return userHibernateDao;
            default:
                return null;
        }
    }
}
