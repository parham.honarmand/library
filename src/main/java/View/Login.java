package View;

import controller.LibraryController;
import controller.UserController;
import model.User;
import utility.Utilities;

public class Login {

    public static boolean isLoggedIn;

    public void login() {

        String username;
        String password;

        System.out.println("Login : ");

        System.out.println("please enter your username");
        username = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your password");
        password = Utilities.getInstance().getStringFromUser();

        User user = LibraryController.getInstance().userController.login(username, password);

        if (user != null) {
            isLoggedIn = true;
            System.out.println("you logged in");
            UserController.currentUser = user;
        } else {
            System.out.println("username or password is wrong");
            login();
        }
//        System.out.println((currentUser!=null ? "now you are login":"username or password is wrong"));
    }
}
