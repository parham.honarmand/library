package repository.HibernateImpl;

import model.User;
import repository.UserRepository;
import utility.JpaUtil;
import utility.UserNotFoundException;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.util.List;

public class UserHibernateDao implements UserRepository {

    protected EntityManager entityManager = JpaUtil.getFactory().createEntityManager();

    public void insert(User user) throws SQLException {

        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    public User searchById(Long id) throws Exception {

        String query = String.format("select * from user where user.id = %d", id);
        User user = (User) entityManager.createNativeQuery(query).getSingleResult();
        return user;
    }

    public List<User> findAll() throws SQLException {
        List<User> userList;
        entityManager.getTransaction().begin();

        String query = String.format("select * from user");
        userList = entityManager.createNativeQuery(query).getResultList();
        entityManager.getTransaction().commit();

        return userList;
    }

    public void update(User user) throws SQLException {
        entityManager.flush();
        entityManager.merge(user);
    }

    public void delete(Long id) throws SQLException {

        String query = String.format("delete from user where user.id = %d", id);
        entityManager.createNativeQuery(query).executeUpdate();
    }

    public User searchByUsername(String username) throws UserNotFoundException , SQLException{

        try {
            String query = String.format("select * from user where user.username = '%s'", username);
            User user = (User) entityManager.createNativeQuery(query).getSingleResult();
        if(user != null)
            return user;
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        throw new UserNotFoundException("this user does not exist");
    }
}
