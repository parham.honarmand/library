package repository;

import model.User;
import utility.UserNotFoundException;

import java.sql.SQLException;

public interface UserRepository extends Repository<User>  {
    User searchByUsername(String username) throws UserNotFoundException, SQLException;
}
