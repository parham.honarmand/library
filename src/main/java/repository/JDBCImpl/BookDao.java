package repository.JDBCImpl;

import connectionManager.ConnectionManager;
import connectionManager.MySqlConnectionManager;
import model.Book;
import repository.BookRepository;
import utility.UserNotFoundException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDao implements BookRepository {
    public ConnectionManager connectionManager = new MySqlConnectionManager();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into books (title, subject, price, penalty, status, borrow_limit)" +
            "values (?, ?, ?, ?, ?, ?)";
    private static final String SELECT_BY_ID_QUERY = "select * from books where books.id = ?";
    private static final String SELECT_BY_TITLE_QUERY = "select * from books where books.title = ?";
    private static final String SELECT_ALL_QUERY = "select * from books";
    private static final String UPDATE_QUERY = "update books set title = ?, subject = ?," +
            " price = ?, penalty = ?, status = ?, borrow_limit = ? where books.id = ?";
    private static final String DELETE_QUERY = "delete from books where books.id = ?";

    public void insert(Book book) throws SQLException {
        System.out.println(INSERT_QUERY);
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setString(1, book.getTitle());
        statement.setString(2, book.getSubject());
        statement.setLong(3, book.getPrice());
        statement.setLong(4, book.getPenalty());
        statement.setInt(5, (book.isStatus() ? 1 : 0));
        statement.setInt(6, book.getBorrow_limit());
        statement.executeUpdate();
    }

    public Book searchById(Long id) throws UserNotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new Book(resultSet.getLong(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getLong(4),
                    resultSet.getLong(5), resultSet.getBoolean(6), resultSet.getInt(7));

        throw new UserNotFoundException("this book does not exist");
    }

    public List<Book> findAll() throws SQLException {
        List<Book> bookList = new ArrayList<Book>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_ALL_QUERY);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            bookList.add(new Book(resultSet.getLong(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getLong(4),
                    resultSet.getLong(5), resultSet.getBoolean(6), resultSet.getInt(7)));

        return bookList;

    }

    public Book searchByTitle(String bookTitle) throws UserNotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_TITLE_QUERY);
        statement.setString(1, bookTitle);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new Book(resultSet.getLong(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getLong(4),
                    resultSet.getLong(5), resultSet.getBoolean(6), resultSet.getInt(7));

        throw new UserNotFoundException("this book does not exist");
    }

    public void update(Book book) throws SQLException {
        System.out.println(UPDATE_QUERY);
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setString(1, book.getTitle());
        statement.setString(2, book.getSubject());
        statement.setLong(3, book.getPrice());
        statement.setLong(4, book.getPenalty());
        statement.setInt(5, (book.isStatus() ? 1 : 0));
        statement.setInt(6, book.getBorrow_limit());
        statement.setLong(7, book.getId());
        statement.executeUpdate();
    }

    public void delete(Long id) throws SQLException {
        System.out.println(DELETE_QUERY);
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }
}
