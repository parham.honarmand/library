package connectionManager;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySqlConnectionManager implements ConnectionManager {

    private Connection connection;
    private static final String USERNAME = "root";
    private static final String PASSWORD = "password";

    public Connection getConnection() {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/library?serverTimezone=UTC", USERNAME, PASSWORD);

        }catch (Exception e){
            System.out.println("ERROR : connection");
        }
        return connection;
    }
}
