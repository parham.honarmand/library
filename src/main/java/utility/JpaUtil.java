package utility;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class JpaUtil {

    private static final String PERSISTANCE_NAME = "LibraryDB";
    private static EntityManagerFactory factory;

    private JpaUtil(){

    }
    public static EntityManagerFactory getFactory(){
        if(factory == null)
            factory = Persistence.createEntityManagerFactory(PERSISTANCE_NAME);
        return factory;
    }

    public static void closeFactory(){
        if(factory != null)
            factory.close();
    }
}
