package service;

import model.User;
import repository.LibraryRepository;
import utility.Encryptor;
import utility.UserNotFoundException;

import java.sql.SQLException;
import java.util.List;

public class UserService {

    public void register(User user) {
        User u = null;
        try {
            u = LibraryRepository.getInstance().getUserDao().searchByUsername(user.getUsername());
            if (u != null)
                return;
        } catch (UserNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
        }
        try {
            user.setPassword(Encryptor.getMd5(user.getPassword()));
            LibraryRepository.getInstance().getUserDao().insert(user);
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
        }
    }

    public User search(Long id) {
        try {
            return LibraryRepository.getInstance().getUserDao().searchById(id);
        } catch (UserNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new User();
    }

    public User login(String username, String password) {
        User u = null;
        try {
            u = LibraryRepository.getInstance().getUserDao().searchByUsername(username);
            if (username.equals(u.getUsername()) && Encryptor.getMd5(password).equals(u.getPassword()))
                return u;
        } catch (UserNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
        }
        return null;
    }

    public List<User> findAll() {
        try {
            System.out.println(LibraryRepository.getInstance() == null);
            return LibraryRepository.getInstance().getUserDao().findAll();
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void update(User user) {
        try {
            LibraryRepository.getInstance().getUserDao().update(user);
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
        }
    }

    public void deleteById(Long id) {
        try {
            LibraryRepository.getInstance().getUserDao().delete(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteByUsername(String username) {
        try {
            User user = LibraryRepository.getInstance().getUserDao().searchByUsername(username);
            deleteById(user.getId());
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
