package model;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@Entity(name = "user")
@Table
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @NotNull
    private String name;

    @Column(name = "username")
    @NotNull
    private String username;

    @Column(name = "password")
    @NotNull
    private String password;

    @Column(name = "national_code")
    @NotNull
    private String nationalCode;

    @Column(name = "creation")
    @NotNull
    private String creation;

    @Column(name = "delay_count")
    @NotNull
    private int delayCount = 0;

    @Column(name = "status")
    @NotNull
    private boolean status = true;

    @Column(name = "budget")
    @NotNull
    private Long budget;

    public User(String name, String username, String password, String nationalCode, String creation, Long budget) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.nationalCode = nationalCode;
        this.creation = creation;
        this.budget = budget;
    }
}
