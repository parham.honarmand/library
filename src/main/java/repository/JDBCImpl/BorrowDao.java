package repository.JDBCImpl;

import connectionManager.ConnectionManager;
import connectionManager.MySqlConnectionManager;
import model.Borrow;
import repository.BorrowRepository;
import utility.UserNotFoundException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BorrowDao implements BorrowRepository {

    public ConnectionManager connectionManager = new MySqlConnectionManager();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into borrows (user_id, book_id, start, end)" +
            "values (?, ?, ?, ?)";
    private static final String SELECT_BY_ID_QUERY = "select * from borrows where borrows.id = ?";
    private static final String SELECT_BY_USER_BOOK_QUERY = "select * from borrows where borrows.user_id = ? and borrows.book_id = ?";
    private static final String SELECT_ALL_QUERY = "select * from borrows";
    private static final String UPDATE_QUERY = "update borrows set user_id = ?, book_id = ?," +
            " start = ?, end = ? where borrows.id = ?";
    private static final String DELETE_QUERY = "delete from borrows where borrows.id = ?";
    private static final String DELETE_BY_USER_ID_AND_BOOK_ID_QUERY = "delete from borrows where borrows.user_id = ? and " +
            "borrows.book_id = ?";

    public void insert(Borrow borrow) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setLong(1, borrow.getUser_id());
        statement.setLong(2, borrow.getBook_id());
        statement.setString(3, borrow.getStart());
        statement.setString(4, borrow.getEnd());
        System.err.println(statement);
        statement.executeUpdate();
    }

    public Borrow searchById(Long id) throws UserNotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new Borrow(resultSet.getLong(1), resultSet.getLong(2),
                    resultSet.getLong(3),resultSet.getString(4),
                    resultSet.getString(5));

        throw new UserNotFoundException("this borrow does not exist");
    }

    public Borrow searchByUserAndBook(Long userId, Long bookId) throws UserNotFoundException, SQLException {
        System.out.println(SELECT_BY_USER_BOOK_QUERY);
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_USER_BOOK_QUERY);
        statement.setLong(1, userId);
        statement.setLong(2, bookId);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new Borrow(resultSet.getLong(1), resultSet.getLong(2),
                    resultSet.getLong(3),resultSet.getString(4),
                    resultSet.getString(5));

        throw new UserNotFoundException("this borrow does not exist");
    }

    public List<Borrow> findAll() throws SQLException {
        List<Borrow> borrowList = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_ALL_QUERY);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            borrowList.add(new Borrow(resultSet.getLong(1), resultSet.getLong(2),
                    resultSet.getLong(3),resultSet.getString(4),
                    resultSet.getString(5)));

        return borrowList;
    }

    public void update(Borrow borrow) throws SQLException {
        System.out.println(UPDATE_QUERY);
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setLong(1, borrow.getUser_id());
        statement.setLong(2, borrow.getBook_id());
        statement.setString(3, borrow.getStart());
        statement.setString(4, borrow.getEnd());
        statement.executeUpdate();
    }

    public void delete(Long id) throws SQLException {
        System.out.println(DELETE_QUERY);
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    public void deleteByUserIdAndBookId(Long user_id, Long book_id) throws SQLException {
        System.out.println(DELETE_BY_USER_ID_AND_BOOK_ID_QUERY);
        statement = connectionManager.getConnection().prepareStatement(DELETE_BY_USER_ID_AND_BOOK_ID_QUERY);
        statement.setLong(1, user_id);
        statement.setLong(2, book_id);
        statement.executeUpdate();
    }
}
